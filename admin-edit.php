<?php
include ('koneksi.php');
?>
<!DOCTYPE html>
<html lang="en">
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Diagnosa Gangguan Rahim</title>
        <!-- Load Roboto font -->
        <link href="http://fonts.googleapis.com/css?family=Roboto:400,300,700&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
        <!-- Load css styles -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/pluton.css" />
        <link href="btn.css" rel="stylesheet">
        <!--[if IE 7]>
            <link rel="stylesheet" type="text/css" href="css/pluton-ie7.css" />
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="css/jquery.cslider.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" />
        <link rel="stylesheet" type="text/css" href="css/animate.css" />
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57.png">
        <link rel="shortcut icon" href="images/ico/favicon.ico">
    </head>
    
    <body>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <a href="#" class="brand">
                        
                    <!-- This is website logo -->
                    </a>
                    <!-- Navigation button, visible on small resolution -->
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <i class="icon-menu"></i>
                    </button>
                    <!-- Main navigation -->
                    <div class="nav-collapse collapse pull-right">
                        <ul class="nav" id="top-navigation">
                            <li class="active"><a href="index.html">Beranda</a></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </div>
                    <!-- End main navigation -->
                </div>
            </div>
        </div>
        <!-- Start home section -->
        <div id="home">
        <!-- Start cSlider --></div>
        <div class="section primary-section" id="service">
            <div class="container">
                <!-- Start title section -->
                <div class="title">
                    <h1>Admin Editing Form</h1>
                    <!-- Section's title goes here -->
                    <p>Form untuk melakukan editing</p>
                    <!--Simple description for section goes here. -->
                </div>
                <div class="row-fluid"></div>
                <div id="sesi1" style="display:block" align="center" style="align-content:center">
                <?php
	error_reporting(E_ALL ^ (E_NOTICE | E_WARNING)); 	
	$message = $_GET['msg'];
	
	if ($message == 'gejala') {
	?>		        
            <div align="center">
            <?php
	error_reporting(E_ALL ^ (E_NOTICE | E_WARNING)); 	
	$message = $_GET['msg'];
	
	if ($message == 'success') {
	?>
	<h2>Action Success!</h2>
	<?php } else if ($message == 'failed') {?>
	<h2>Action Error</h2>
	<?php } ?>
            <!--Edit Main Content Area here-->
            <h1>Edit Data Gejala</h1>
            <div class="media-object" align="center">
            <?php
			$id = $_GET['id'];
			$query = mysql_query("select * from uterusdb.gejala where id='$id'");
      
      		$data = mysql_fetch_array($query); 
			?>
            
            <form method="post" action="admin-update.php">
            <input name="id" value="<?php echo $data['id'] ?>" type="hidden">
            <label>Kode Gejala</label><br/>
            <input name="kode" required style="text-align:center" value="<?php echo $data['kode'] ?>"><br/>
            <label>Nama Gejala</label><br/>
            <input name="gejala" required style="text-align:center" value="<?php echo $data['gejala'] ?>"><br/>
            <label>Bobot Pakar</label><br/>
            <input name="bobot" required style="text-align:center" value="<?php echo $data['bobot'] ?>"><br/>
            
            <button type="submit" class="myButton">Update</button><br/>
            </form>
            
            <a href="admin-login.php"><button class="myButton">Kembali</button></a>
            
                    
            </div>
             </div>
             <?php } else if ($message == 'diagnosa') {?>
             <div class="media-body" align="center">
            <?php
	error_reporting(E_ALL ^ (E_NOTICE | E_WARNING)); 	
	$message = $_GET['msg'];
	
	if ($message == 'success') {
	?>
	<h2>Action Success!</h2>
	<?php } else if ($message == 'failed') {?>
	<h2>Action Error</h2>
	<?php } ?>
            <!--Edit Main Content Area here-->
            <h1>Edit Data Diagnosa</h1>
            <div class="media-object" align="center">
            <?php
			$id = $_GET['id'];
			$query = mysql_query("select * from uterusdb.diag where id='$id'");
      
      		$data = mysql_fetch_array($query); 
			?>
            
            <form method="post" action="admin-update2.php">
            <input name="id" value="<?php echo $data['id'] ?>" type="hidden">
            <label>Kode Diagnosa</label><br/>
            <input name="kode" required style="text-align:center" value="<?php echo $data['kode'] ?>"><br/>
            <label>Diagnosa</label><br/>
            <input name="nama" required style="text-align:center" value="<?php echo $data['diagnosa'] ?>"><br/>
            
            <button type="submit" class="myButton">Update</button><br/>
            </form>
            
            <a href="admin-login.php"><button class="myButton">Kembali</button></a>
          </div>
          </div>
          
          <?php } else if ($message == 'addGejala') {?>
             <div class="media-body" align="center">
             <h1>Tambah Data Gejala</h1>
            <form method="post" action="admin-tambah.php">
            <label>Kode Gejala</label><br/>
            <input name="kode" required style="text-align:center"><br/>
            <label>Gejala</label><br/>
            <input name="gejala" required style="text-align:center"><br/>
            <label>Bobot</label><br/>
            <input name="bobot" required style="text-align:center"><br/><br/><br/>
            
            <button type="submit" class="myButton">Tambahkan</button><br/>
            </form>
            
            <a href="admin-login.php"><button class="myButton">Kembali</button></a>
        </div>
        
        <?php } else if ($message == 'addDiagnosa') {?>
             <div class="media-body" align="center">
             <h1>Tambah Data Diagnosa</h1>
            <form method="post" action="admin-tambah2.php">
            <label>Kode Diagnosa</label><br/>
            <input name="kode" required style="text-align:center"><br/>
            <label>Nama Diagnosa</label><br/>
            <input name="diagnosa" required style="text-align:center"><br/><br/><br/>
            
            <button type="submit" class="myButton">Tambahkan</button><br/>
            </form>
            
            <a href="admin-login.php"><button class="myButton">Kembali</button></a>
            <?php } ?>
        </div>
            </div>
        </div>
        <div class="section third-section"></div>
        <div class="footer">
            <p>&copy; 2018 MANTRI WIRA PUTRA</p>
        </div>
        <div class="scrollup">
            <a href="#">
                <i class="icon-up-open"></i>
            </a>
        </div>
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>