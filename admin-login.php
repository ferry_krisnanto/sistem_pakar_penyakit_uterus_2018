<?php
include ('koneksi.php');
?>
<!DOCTYPE html>
<html lang="en">
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Diagnosa Gangguan Rahim</title>
        <!-- Load Roboto font -->
        <link href="http://fonts.googleapis.com/css?family=Roboto:400,300,700&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
        <!-- Load css styles -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/pluton.css" />
        <link href="btn.css" rel="stylesheet">
        <!--[if IE 7]>
            <link rel="stylesheet" type="text/css" href="css/pluton-ie7.css" />
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="css/jquery.cslider.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" />
        <link rel="stylesheet" type="text/css" href="css/animate.css" />
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57.png">
        <link rel="shortcut icon" href="images/ico/favicon.ico">
        
<script>

function toggle_visibility (id)
	{
		var e = document.getElementById(id);
		if (e.style.display == 'block')
		e.style.display = 'none';
		else
		e.style.display = 'block';
	}
</script>
<style>
	
			th {
    			background-color: #2BA6FF;
    			color: white;
			}
			table, th, td {
    			border: 1px solid black;
			}
			td {
				vertical-align:middle;
			}
			th, td {
				padding:8px;
			}
			tr:nth-child(even){background-color: #f2f2f2};
			
			
</style>
    </head>
    
    <body>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <a href="#" class="brand">
                        
                    <!-- This is website logo -->
                    </a>
                    <!-- Navigation button, visible on small resolution -->
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <i class="icon-menu"></i>
                    </button>
                    <!-- Main navigation -->
                    <div class="nav-collapse collapse pull-right">
                        <ul class="nav" id="top-navigation">
                            <li class="active"><a href="logout.php">Logout</a></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </div>
                    <!-- End main navigation -->
                </div>
            </div>
        </div>
        <!-- Start home section -->
        <div id="home">
        <!-- Start cSlider --></div>
        <div class="section primary-section" id="service">
            <div class="container">
                <!-- Start title section -->
                <div class="title">
                    <h1>Admin Panel Control</h1>
                    <!-- Section's title goes here -->
                    <p>Silahkan Lakukan Editing melalui panel ini</p>
                    <!--Simple description for section goes here. -->
                </div>
                <div class="row-fluid"></div>
                <div id="sesi1" style="display:block" align="center" style="align-content:center">
                <?php
	error_reporting(E_ALL ^ (E_NOTICE | E_WARNING)); 	
	$message = $_GET['msg'];
	
	if ($message == 'success') {
	?>
	<h2>Action Success!</h2>
	<?php } else if ($message == 'failed') {?>
	<h2>Action Error</h2>
	<?php } ?>
    
          <button class="myButton" onclick="toggle_visibility ('gejala')">Edit Database Gejala</button><br />
          <div id="gejala" style="display:none">
    <a href="admin-edit.php?msg=addGejala" style="left:0"><button class="myButton">Insert</button></a>
    
    	<table bordercolor="#737373" style="background-color:#FFFFFF; padding:5px; color:#000000">
                    <thead>
                    	<tr>
                    		<th>No.</th>
                            <th>Kode Gejala</th>
                        	<th>Gejala</th>
                        	<th>Bobot</th>
                            <th>Aksi</th>
                    	</tr>
                    </thead>
                    <tbody>
                    <?php
					$a = "select * from uterusdb.gejala";
	 				$query = mysql_query($a);
  	 				$i = 1;		
	 				while ($data = mysql_fetch_array($query)) {
	 				?>
                    	<tr class="<?php if ($i % 2 == 0) { echo "odd"; } else { echo "even"; } ?>">
							<td><?php echo $i."."; ?></td>
							<td align="center">
							<?php 
							echo $data['kode'];
							?>
                         </td>
                         <td align="left"><?php echo $data['gejala']; ?> </td>
                         <td align="center"><?php echo $data['bobot']; ?> </td>
                         <td align="center">
                         <div>
                         <a href="admin-edit.php?msg=gejala&id=<?php echo $data['id'];?>">Edit</a>
                         <a href="admin-delete.php?id=<?php echo $data['id'];?>">Delete</a>
                         </div>
						 </td>
                         </tr>
                         <?php 
							$i++;
	 						}
							?>
                    </tbody>
                    </table>
                    
          </div><br />
          <button class="myButton" onclick="toggle_visibility ('diagnosa')">Edit Database Diagnosa</button><br />
          <div id="diagnosa" style="display:none" align="center">
          
    <a href="admin-edit.php?msg=addDiagnosa" style="left:0"><button class="myButton">Insert</button></a>
    
    	<table bordercolor="#737373" style="background-color:#FFFFFF; padding:5px; color:#000000">
                    <thead>
                    	<tr>
                    		<th>No.</th>
                            <th>Kode Diagnosa</th>
                        	<th>Diagnosa</th>
                            <th>Aksi</th>
                    	</tr>
                    </thead>
                    <tbody>
                    <?php
					$a = "select * from uterusdb.diag";
	 				$query = mysql_query($a);
  	 				$i = 1;		
	 				while ($data = mysql_fetch_array($query)) {
	 				?>
                    	<tr class="<?php if ($i % 2 == 0) { echo "odd"; } else { echo "even"; } ?>">
							<td><?php echo $i."."; ?></td>
							<td align="center">
							<?php 
							echo $data['kode'];
							?>
                         </td>
                         <td align="left"><?php echo $data['diagnosa']; ?> </td>
                         <td align="center">
                         <div>
                         <a href="admin-edit.php?msg=diagnosa&id=<?php echo $data['id'];?>">Edit</a>
                         <a href="admin-delete2.php?id=<?php echo $data['id'];?>">Delete</a>
                         </div>
						 </td>
                         </tr>
                         <?php 
							$i++;
	 						}
							?>
                    </tbody>
                    </table>

                    
          </div><br />
            </div>
        </div>
        <div class="section third-section"></div>
        <div class="footer">
            <p>&copy; 2018 MANTRI WIRA PUTRA</p>
        </div>
        <div class="scrollup">
            <a href="#">
                <i class="icon-up-open"></i>
            </a>
        </div>
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>