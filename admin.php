<?php
include ('koneksi.php');
?>
<!DOCTYPE html>
<html lang="en">
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Diagnosa Gangguan Rahim</title>
        <!-- Load Roboto font -->
        <link href="http://fonts.googleapis.com/css?family=Roboto:400,300,700&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
        <!-- Load css styles -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/pluton.css" />
        <link href="btn.css" rel="stylesheet">
        <!--[if IE 7]>
            <link rel="stylesheet" type="text/css" href="css/pluton-ie7.css" />
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="css/jquery.cslider.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" />
        <link rel="stylesheet" type="text/css" href="css/animate.css" />
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57.png">
        <link rel="shortcut icon" href="images/ico/favicon.ico">
    </head>
    
    <body>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <a href="#" class="brand">
                        
                    <!-- This is website logo -->
                    </a>
                    <!-- Navigation button, visible on small resolution -->
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <i class="icon-menu"></i>
                    </button>
                    <!-- Main navigation -->
                    <div class="nav-collapse collapse pull-right">
                        <ul class="nav" id="top-navigation">
                            <li class="active"><a href="index.html">Beranda</a></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </div>
                    <!-- End main navigation -->
                </div>
            </div>
        </div>
        <!-- Start home section -->
        <div id="home">
        <!-- Start cSlider --></div>
        <div class="section primary-section" id="service">
            <div class="container">
                <!-- Start title section -->
                <div class="title">
                    <h1>Admin Login Form</h1>
                    <!-- Section's title goes here -->
                    <p>Silahkan isi Username dan Password anda</p>
                    <!--Simple description for section goes here. -->
                </div>
                <div class="row-fluid"></div>
                <div id="sesi1" style="display:block" align="center" style="align-content:center">
                <?php
				error_reporting(E_ALL ^ (E_NOTICE | E_WARNING)); 	
				$message = $_GET['msg'];
	
				if ($message == 'success') {
		  ?>
		  <div class="info">Success</div>
		  <?php } else if ($message == 'failed1') {?>
		  <h2>Password dan username tidak boleh kosong!</h2>
          <?php } else if ($message == 'failed2') {?>
		  <h2>Username tidak boleh kosong!</h2>
          <?php } else if ($message == 'failed3') {?>
		  <h2>Anda bukan Admin!</h2>
          <?php } else if ($message == 'failed4') {?>
		  <h2>Password atau Username anda bermasalah!</h2>
		  <?php } ?>
          <form method="post" action="login.php">
          <h3>Username</h3>
          <input type="text" style="text-align:center" name="username" />
          <h3>Password</h3>
          <input type="password" style="text-align:center" name="password" /><br />
          <button type="submit" class="myButton">Login!</button>
          </form>
            </div>
        </div>
        <div class="section third-section"></div>
        <div class="footer">
            <p>&copy; 2018 MANTRI WIRA PUTRA</p>
        </div>
        <div class="scrollup">
            <a href="#">
                <i class="icon-up-open"></i>
            </a>
        </div>
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>