<?php
include ('koneksi.php');
session_start();
if($_SESSION['id_user'] == NULL){
	header("location: login_user.php");
}
?>
<!DOCTYPE html>
<html lang="en">
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Diagnosa Gangguan Rahim</title>
        <!-- Load Roboto font -->
        <link href="http://fonts.googleapis.com/css?family=Roboto:400,300,700&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
        <!-- Load css styles -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/pluton.css" />
        <link href="btn.css" rel="stylesheet">
        <!--[if IE 7]>
            <link rel="stylesheet" type="text/css" href="css/pluton-ie7.css" />
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="css/jquery.cslider.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" />
        <link rel="stylesheet" type="text/css" href="css/animate.css" />
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57.png">
        <link rel="shortcut icon" href="images/ico/favicon.ico">

        <style type="text/css">
        	.tombol{
			  background:#181A1C;
			  color:white;
			  border-top:0;
			  border-left:0;
			  border-right:0;
			  border-bottom:0;
			  padding:10px 20px;
			  text-decoration:none;
			  font-family:sans-serif;
			  font-size:11pt;
			}
        </style>

    </head>
    
    <body>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <a href="#" class="brand">
                        
                    <!-- This is website logo -->
                    </a>
                    <!-- Navigation button, visible on small resolution -->
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <i class="icon-menu"></i>
                    </button>
                    <!-- Main navigation -->
                    <div class="nav-collapse collapse pull-right">
                        <ul class="nav" id="top-navigation">
                            <li class="active"><a href="logout.php">Logout</a></li>
                            <li><a href="#"><button class="tombol" onclick="myFunction()">Cetak</button></a></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </div>
                    <!-- End main navigation -->
                </div>
            </div>
        </div>
        <!-- Start home section -->
        <div id="home">
        <!-- Start cSlider --></div>
        <div class="section primary-section" id="service">
            <div class="container">
                <!-- Start title section -->
                <div class="title">
                    <h1>Hasil Diagnosa Anda</h1>
                    <!-- Section's title goes here -->
                    <p>Silahkan Baca Artikel untuk keterangan lebih lanjut</p>
                    <!--Simple description for section goes here. -->
                </div>
                <div class="row-fluid"></div>
                <div id="sesi1" style="display:block" align="center" style="align-content:center">
                <?php
include ('koneksi.php');

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "uterusdb";

// Koneksi baru via mysql improve
$conn = new mysqli($servername, $username, $password, $dbname);
// Check koneksi
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//--------------------------------------------------------------------------------------------------------!
$sql = "SELECT * FROM uterusdb.tmp_jawab";
$result = $conn->query($sql);

if ($result->num_rows > 0 ) {
    // output data dari masing2 baris
    while($row = $result->fetch_assoc()) {
		$gejala   =$row["cf_pakar"]*$row["jawaban"];
		$kode = $row['kode'];
		$sql2 = "INSERT INTO `uterusdb`.`tmp_cf` (`kode`, `cf_total`, `id_user`) VALUES ('$kode', '$gejala', ".$_SESSION['id_user'].")";
		$conn->query($sql2);
	}
}
//--------------------------------------------------------------Fungsi Dasar------------------------------!

function cfx($data) {
	$hasil = $data * 100 .'%';
	return $hasil;
}

function cf_combine($cf_data1,$cf_data2){
	$hasil = $cf_data1 + $cf_data2 * (1 - $cf_data1);
	return $hasil;
}

function diagnosa($diagnosa){
	$query =  mysql_query("SELECT * FROM uterusdb.diag WHERE kode = '$diagnosa'");
	$array = array();
	while($row = mysql_fetch_assoc($query)){
		$array[]=$row;
	}
	
	echo 'Kemungkinan Penyakit diderita :'.'<br>'.$array[0]['diagnosa'].'<br>';
}

function MB($a,$b) {
	$max = (max($a,$b)-$b)/( max (1,0) - $b);
	$min = (min($a,$b)-$b)/( min (1,0) - $b);
	$hasil = max($max,$min) * 100 .'%';
	return $hasil;
}

//------------------------------------------------------------------END-----------------------------------!

//------------------------------------------------------------Pengambilan Data 1--------------------------!

$diag1 = mysql_query("SELECT kode, cf_total FROM uterusdb.tmp_cf WHERE kode IN ('GJ01', 'GJ02', 'GJ03', 'GJ04', 'GJ05', 'GJ06', 'GJ07', 'GJ08', 'GJ09', 'GJ10')");//--10 Gejala
$diag2 = mysql_query("SELECT kode, cf_total FROM uterusdb.tmp_cf WHERE kode IN ('GJ11', 'GJ12', 'GJ13')");//--3 Gejala
$diag3 = mysql_query("SELECT kode, cf_total FROM uterusdb.tmp_cf WHERE kode IN ('GJ14', 'GJ15', 'GJ16', 'GJ17', 'GJ18', 'GJ19', 'GJ20')");//7 Gejala
$diag4 = mysql_query("SELECT kode, cf_total FROM uterusdb.tmp_cf WHERE kode IN ('GJ21', 'GJ22', 'GJ23')");//--3 Gejala
$diag5 = mysql_query("SELECT kode, cf_total FROM uterusdb.tmp_cf WHERE kode IN ('GJ24', 'GJ25', 'GJ26')");//--3 Gejala
$diag6 = mysql_query("SELECT kode, cf_total FROM uterusdb.tmp_cf WHERE kode IN ('GJ27', 'GJ28', 'GJ29', 'GJ30', 'GJ31')");//--5 Gejala
$diag7 = mysql_query("SELECT kode, cf_total FROM uterusdb.tmp_cf WHERE kode IN ('GJ32', 'GJ33', 'GJ34')");//--3 Gejala

//------------------------------------------------------------------END-----------------------------------!

//------------------------------------------------------------Pengambilan Data 2--------------------------!

$case1 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ01'");
$case2 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ02'");
$case3 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ03'");
$case4 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ04'");
$case5 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ05'");
$case6 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ06'");
$case7 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ07'");
$case8 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ08'");
$case9 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ09'");
$case10 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ10'");
$case11 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ11'");
$case12 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ12'");
$case13 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ13'");
$case14 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ14'");
$case15 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ15'");
$case16 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ16'");
$case17 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ17'");
$case18 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ18'");
$case19 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ19'");
$case20 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ20'");
$case21 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ21'");
$case22 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ22'");
$case23 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ23'");
$case24 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ24'");
$case25 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ25'");
$case26 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ26'");
$case27 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ27'");
$case28 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ28'");
$case29 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ29'");
$case30 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ30'");
$case31 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ31'");
$case32 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ32'");
$case33 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ33'");
$case34 = mysql_query("SELECT jawaban FROM uterusdb.tmp_jawab WHERE kode='GJ34'");

//------------------------------------------------------------------END-----------------------------------!

//------------------------------------------------------------Rule Per-Gejala-----------------------------!

if (mysql_num_rows($diag1) == 10){
	$diagnosa = 'D1';
	$Arr = array();
	while ($rowData = mysql_fetch_assoc($diag1)){
		$Arr[] = $rowData;
	}
	$data1 = $Arr[0]['cf_total'];
	$data2 = $Arr[1]['cf_total'];
	$data3 = $Arr[2]['cf_total'];
	$data4 = $Arr[3]['cf_total'];
	$data5 = $Arr[4]['cf_total'];
	$data6 = $Arr[5]['cf_total'];
	$data7 = $Arr[6]['cf_total'];
	$data8 = $Arr[7]['cf_total'];
	$data9 = $Arr[8]['cf_total'];
	$data10 = $Arr[9]['cf_total'];
	
	$cf_old1 = cf_combine($data1,$data2);
	$cf_old2 = cf_combine($cf_old1,$data3);
	$cf_old3 = cf_combine($cf_old2,$data4);
	$cf_old4 = cf_combine($cf_old3,$data5);
	$cf_old5 = cf_combine($cf_old4,$data6);
	$cf_old6 = cf_combine($cf_old5,$data7);
	$cf_old7 = cf_combine($cf_old6,$data8);
	$cf_old8 = cf_combine($cf_old7,$data9);
	$cf_old9 = cf_combine($cf_old8,$data10);
	
	$cf_hasil = $cf_old9 * 100;
	echo 'Nilai persentase CF = '.$cf_hasil.'%'.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//--------------------------------------------------------------------------------- END of Kondisi 1

else if (mysql_num_rows($diag2) == 3){
	$diagnosa = 'D2';
	$Arr = array();
	while ($rowData = mysql_fetch_assoc($diag2)){
		$Arr[] = $rowData;
	}
	$data1 = $Arr[0]['cf_total'];
	$data2 = $Arr[1]['cf_total'];
	$data3 = $Arr[2]['cf_total'];
		
	$cf_old1 = cf_combine($data1,$data2);
	$cf_old2 = cf_combine($cf_old1,$data3);
	
	$cf_hasil = $cf_old2 * 100;
	echo 'Nilai persentase CF = '.$cf_hasil.'%'.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------END of Kondisi 2

else if (mysql_num_rows($diag3) == 7){
	$diagnosa = 'D3';
	$Arr = array();
	while ($rowData = mysql_fetch_assoc($diag3)){
		$Arr[] = $rowData;
	}
	$data1 = $Arr[0]['cf_total'];
	$data2 = $Arr[1]['cf_total'];
	$data3 = $Arr[2]['cf_total'];
	$data4 = $Arr[3]['cf_total'];
	$data5 = $Arr[4]['cf_total'];
	$data6 = $Arr[5]['cf_total'];
	$data7 = $Arr[6]['cf_total'];
	
	$cf_old1 = cf_combine($data1,$data2);
	$cf_old2 = cf_combine($cf_old1,$data3);
	$cf_old3 = cf_combine($cf_old2,$data4);
	$cf_old4 = cf_combine($cf_old3,$data5);
	$cf_old5 = cf_combine($cf_old4,$data6);
	$cf_old6 = cf_combine($cf_old5,$data7);
	
	$cf_hasil = $cf_old6 * 100;
	echo 'Nilai persentase CF = '.$cf_hasil.'%'.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------END of Kondisi 3

else if (mysql_num_rows($diag4) == 3){
	$diagnosa = 'D4';
	$Arr = array();
	while ($rowData = mysql_fetch_assoc($diag4)){
		$Arr[] = $rowData;
	}
	$data1 = $Arr[0]['cf_total'];
	$data2 = $Arr[1]['cf_total'];
	$data3 = $Arr[2]['cf_total'];
	
	$cf_old1 = cf_combine($data1,$data2);
	$cf_old2 = cf_combine($cf_old1,$data3);
	
	$cf_hasil = $cf_old2 * 100;
	echo 'Nilai persentase CF = '.$cf_hasil.'%'.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------END of Kondisi 4

else if (mysql_num_rows($diag5) == 3){
	$diagnosa = 'D5';
	$Arr = array();
	while ($rowData = mysql_fetch_assoc($diag5)){
		$Arr[] = $rowData;
	}
	$data1 = $Arr[0]['cf_total'];
	$data2 = $Arr[1]['cf_total'];
	$data3 = $Arr[2]['cf_total'];
	
	$cf_old1 = cf_combine($data1,$data2);
	$cf_old2 = cf_combine($cf_old1,$data3);
	
	$cf_hasil = $cf_old2 * 100;
	echo 'Nilai persentase CF = '.$cf_hasil.'%'.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------END of Kondisi 5

else if (mysql_num_rows($diag6) == 5){
	$diagnosa = 'D6';
	$Arr = array();
	while ($rowData = mysql_fetch_assoc($diag6)){
		$Arr[] = $rowData;
	}
	$data1 = $Arr[0]['cf_total'];
	$data2 = $Arr[1]['cf_total'];
	$data3 = $Arr[2]['cf_total'];
	$data4 = $Arr[3]['cf_total'];
	$data5 = $Arr[4]['cf_total'];
	
	$cf_old1 = cf_combine($data1,$data2);
	$cf_old2 = cf_combine($cf_old1,$data3);
	$cf_old3 = cf_combine($cf_old2,$data4);
	$cf_old4 = cf_combine($cf_old3,$data5);
	
	$cf_hasil = $cf_old4 * 100;
	echo 'Nilai persentase CF = '.$cf_hasil.'%'.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------END of Kondisi 6

else if (mysql_num_rows($diag7) == 3){
	$diagnosa = 'D7';
	$Arr = array();
	while ($rowData = mysql_fetch_assoc($diag7)){
		$Arr[] = $rowData;
	}
	$data1 = $Arr[0]['cf_total'];
	$data2 = $Arr[1]['cf_total'];
	$data3 = $Arr[2]['cf_total'];
	
	$cf_old1 = cf_combine($data1,$data2);
	$cf_old2 = cf_combine($cf_old1,$data3);
	
	$cf_hasil = $cf_old2 * 100;
	echo 'Nilai persentase CF = '.$cf_hasil.'%'.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------END of Kondisi 7

else if (mysql_num_rows($case1) == 1){
	$Arr = mysql_fetch_array($case1);
	$data = $Arr['jawaban'];
	$cf1 = 0.4;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D1';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-1

else if (mysql_num_rows($case2) == 1){
	$Arr = mysql_fetch_array($case2);
	$data = $Arr['jawaban'];
	$cf1 = '-0.1';
	$hasil = MB($data,$cf1);
	$diagnosa = 'D1';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-2
else if (mysql_num_rows($case3) == 1){
	$Arr = mysql_fetch_array($case3);
	$data = $Arr['jawaban'];
	$cf1 = 0.9;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D1';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-3
else if (mysql_num_rows($case4) == 1){
	$Arr = mysql_fetch_array($case4);
	$data = $Arr['jawaban'];
	$cf1 = 0.6;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D1';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-4
else if (mysql_num_rows($case5) == 1){
	$Arr = mysql_fetch_array($case5);
	$data = $Arr['jawaban'];
	$cf1 = 0.6;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D1';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-5
else if (mysql_num_rows($case6) == 1){
	$Arr = mysql_fetch_array($case6);
	$data = $Arr['jawaban'];
	$cf1 = 0.6;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D1';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-6
else if (mysql_num_rows($case7) == 1){
	$Arr = mysql_fetch_array($case7);
	$data = $Arr['jawaban'];
	$cf1 = 0.6;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D1';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-7
else if (mysql_num_rows($case8) == 1){
	$Arr = mysql_fetch_array($case8);
	$data = $Arr['jawaban'];
	$cf1 = 0.9;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D1';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-8
else if (mysql_num_rows($case9) == 1){
	$Arr = mysql_fetch_array($case9);
	$data = $Arr['jawaban'];
	$cf1 = 0.6;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D1';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-9
else if (mysql_num_rows($case10) == 1){
	$Arr = mysql_fetch_array($case10);
	$data = $Arr['jawaban'];
	$cf1 = 0.6;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D1';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-10
else if (mysql_num_rows($case11) == 1){
	$Arr = mysql_fetch_array($case11);
	$data = $Arr['jawaban'];
	$cf1 = 0.9;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D2';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-11
else if (mysql_num_rows($case12) == 1){
	$Arr = mysql_fetch_array($case12);
	$data = $Arr['jawaban'];
	$cf1 = 0.6;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D2';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-12
else if (mysql_num_rows($case13) == 1){
	$Arr = mysql_fetch_array($case13);
	$data = $Arr['jawaban'];
	$cf1 = 0.4;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D2';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-13
else if (mysql_num_rows($case14) == 1){
	$Arr = mysql_fetch_array($case14);
	$data = $Arr['jawaban'];
	$cf1 = 0.8;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D3';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-14
else if (mysql_num_rows($case15) == 1){
	$Arr = mysql_fetch_array($case15);
	$data = $Arr['jawaban'];
	$cf1 = 0.6;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D3';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-15
else if (mysql_num_rows($case16) == 1){
	$Arr = mysql_fetch_array($case16);
	$data = $Arr['jawaban'];
	$cf1 = 0.6;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D3';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-16
else if (mysql_num_rows($case17) == 1){
	$Arr = mysql_fetch_array($case17);
	$data = $Arr['jawaban'];
	$cf1 = 0.6;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D3';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-17
else if (mysql_num_rows($case18) == 1){
	$Arr = mysql_fetch_array($case18);
	$data = $Arr['jawaban'];
	$cf1 = 0.6;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D3';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-18
else if (mysql_num_rows($case19) == 1){
	$Arr = mysql_fetch_array($case19);
	$data = $Arr['jawaban'];
	$cf1 = 0.9;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D3';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-19
else if (mysql_num_rows($case20) == 1){
	$Arr = mysql_fetch_array($case20);
	$data = $Arr['jawaban'];
	$cf1 = 0.9;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D3';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-20
else if (mysql_num_rows($case21) == 1){
	$Arr = mysql_fetch_array($case21);
	$data = $Arr['jawaban'];
	$cf1 = 0.9;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D4';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-21
else if (mysql_num_rows($case22) == 1){
	$Arr = mysql_fetch_array($case22);
	$data = $Arr['jawaban'];
	$cf1 = 0.6;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D4';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-22
else if (mysql_num_rows($case23) == 1){
	$Arr = mysql_fetch_array($case23);
	$data = $Arr['jawaban'];
	$cf1 = 0.9;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D4';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-23
else if (mysql_num_rows($case24) == 1){
	$Arr = mysql_fetch_array($case24);
	$data = $Arr['jawaban'];
	$cf1 = 0.8;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D5';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-24
else if (mysql_num_rows($case25) == 1){
	$Arr = mysql_fetch_array($case25);
	$data = $Arr['jawaban'];
	$cf1 = 0.6;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D5';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-25
else if (mysql_num_rows($case26) == 1){
	$Arr = mysql_fetch_array($case26);
	$data = $Arr['jawaban'];
	$cf1 = 0.1;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D5';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-26
else if (mysql_num_rows($case27) == 1){
	$Arr = mysql_fetch_array($case27);
	$data = $Arr['jawaban'];
	$cf1 = 0.9;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D6';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-27
else if (mysql_num_rows($case28) == 1){
	$Arr = mysql_fetch_array($case28);
	$data = $Arr['jawaban'];
	$cf1 = 0.9;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D6';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-28
else if (mysql_num_rows($case29) == 1){
	$Arr = mysql_fetch_array($case29);
	$data = $Arr['jawaban'];
	$cf1 = 0.9;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D6';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-29
else if (mysql_num_rows($case30) == 1){
	$Arr = mysql_fetch_array($case30);
	$data = $Arr['jawaban'];
	$cf1 = 0.4;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D6';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-30
else if (mysql_num_rows($case31) == 1){
	$Arr = mysql_fetch_array($case31);
	$data = $Arr['jawaban'];
	$cf1 = 0.4;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D6';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-31
else if (mysql_num_rows($case32) == 1){
	$Arr = mysql_fetch_array($case32);
	$data = $Arr['jawaban'];
	$cf1 = 0.1;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D7';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-32
else if (mysql_num_rows($case33) == 1){
	$Arr = mysql_fetch_array($case33);
	$data = $Arr['jawaban'];
	$cf1 = 0.2;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D7';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-33
else if (mysql_num_rows($case34) == 1){
	$Arr = mysql_fetch_array($case34);
	$data = $Arr['jawaban'];
	$cf1 = 0.1;
	$hasil = MB($data,$cf1);
	$diagnosa = 'D7';
	echo 'Nilai persentase CF = '.$hasil.'<br>';
	diagnosa($diagnosa).'<br><br>';
}//---------------------------------------------------------------------------------Kondisi Case Gejala ke-34
//------------------------------------------------------------------END-----------------------------------!

?>
            </div>
        </div>
        <div class="section third-section"></div>
		
        <div class="footer">
            <p>&copy; 2018 MANTRI WIRA PUTRA</p>
        </div>
        <div class="scrollup">
            <a href="#">
                <i class="icon-up-open"></i>
            </a>
        </div>
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
        <script type="text/javascript" src="js/app.js"></script>
<?php
	$_SESSION['id_user'] = NULL;
?>
    </body>
</html>
<script>
	// window.print();

	function myFunction() {
    window.print();
}
</script>