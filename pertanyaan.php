<?php
include ('koneksi.php');
session_start();
if($_SESSION['id_user'] == NULL){
	header("location: login_user.php");
}
?>
<!DOCTYPE html>
<html lang="en">
    
    <head>
<script>
function hiddenShow (id,id2)
{
	var e = document.getElementById(id);
	var e2 = document.getElementById(id2);
	if (e.style.display == 'block')
		e.style.display = 'none';
		e2.style.display = 'block';
}
function checkbox (id) {
	var c = document.getElementById(id)
	if(c.checked){
		c.value = 'ya';
	} else {
		c.value = 'tidak';
	}
}
</script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Diagnosa Gangguan Rahim</title>
        <!-- Load Roboto font -->
        <link href="http://fonts.googleapis.com/css?family=Roboto:400,300,700&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
        <!-- Load css styles -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/pluton.css" />
        <link href="btn.css" rel="stylesheet">
        <!--[if IE 7]>
            <link rel="stylesheet" type="text/css" href="css/pluton-ie7.css" />
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="css/jquery.cslider.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" />
        <link rel="stylesheet" type="text/css" href="css/animate.css" />
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57.png">
        <link rel="shortcut icon" href="images/ico/favicon.ico">
    </head>
    
    <body>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <a href="#" class="brand">
                        
                    <!-- This is website logo -->
                    </a>
                    <!-- Navigation button, visible on small resolution -->
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <i class="icon-menu"></i>
                    </button>
                    <!-- Main navigation -->
                    <div class="nav-collapse collapse pull-right">
                        <ul class="nav" id="top-navigation">
                            <li class="active"><a href="index.html">Beranda</a></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </div>
                    <!-- End main navigation -->
                </div>
            </div>
        </div>
        <!-- Start home section -->
        <div id="home">
        <!-- Start cSlider --></div>
        <div class="section primary-section" id="service">
            <div class="container">
                <!-- Start title section -->
                <div class="title">
                    <h1>Anda sedang melakukan diagnosa</h1>
                    <!-- Section's title goes here -->
                    <p>Berikan checklist terhadap gejala yang anda rasakan</p>
                    <!--Simple description for section goes here. -->
                </div>
                <div class="row-fluid"></div>
                <div id="sesi1" style="display:block" align="center" style="align-content:center">
                <form method="post" action="tahap1.php">
                    <table style="padding:5px">
                    <thead>
                    	<tr>
                    		<th>No.</th>
                        	<th>Gejala</th>
                    	</tr>
                    </thead>
                    <tbody>
                    <?php
	 				$query = mysql_query("select * from uterusdb.gejala");
  	 				$i = 1;		
	 				while ($data =mysql_fetch_array($query)) {
	 				?>
                    	<tr class="<?php if ($i % 2 == 0) { echo "odd"; } else { echo "even"; } ?>">
							<td><?php echo $i."."; ?></td>
							<td><input name="data[]" type="checkbox" value="<?php echo $data['id']; ?>">&nbsp;
							<?php 
							echo $data['gejala'];
							?>
                         </td>
                         </tr>
                         <?php 
							$i++;
	 						}
							?>
                    </tbody>
                    </table>
					</br></br>
                  <a class="button" style="right:0"><button type="submit" class="myButton">Selanjutnya</button></a>
                </form>
            </div>
        </div>
        <div class="section third-section"></div>
        <div class="footer">
            <p>&copy; 2018 MANTRI WIRA PUTRA</p>
        </div>
        <div class="scrollup">
            <a href="#">
                <i class="icon-up-open"></i>
            </a>
        </div>
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>