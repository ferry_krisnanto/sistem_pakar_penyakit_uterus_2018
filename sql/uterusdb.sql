-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2018 at 11:53 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uterusdb`
--
CREATE DATABASE IF NOT EXISTS `uterusdb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `uterusdb`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `username` char(30) NOT NULL,
  `password` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`) VALUES
('admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `diag`
--

CREATE TABLE `diag` (
  `id` int(11) NOT NULL,
  `kode` varchar(45) DEFAULT NULL,
  `diagnosa` varchar(45) DEFAULT NULL,
  `penanganan` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `diag`
--

INSERT INTO `diag` (`id`, `kode`, `diagnosa`, `penanganan`) VALUES
(1, 'D1', 'MIOM', NULL),
(2, 'D2', 'Nyeri Menstruasi', NULL),
(3, 'D3', 'Endometriosis', NULL),
(4, 'D4', 'Cervical Dysplasia', NULL),
(5, 'D5', 'Prolaps Uteri', NULL),
(6, 'D6', 'Endometrium', NULL),
(7, 'D7', 'Polip Servilks', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gejala`
--

CREATE TABLE `gejala` (
  `id` int(11) NOT NULL,
  `kode` varchar(45) DEFAULT NULL,
  `gejala` varchar(200) DEFAULT NULL,
  `bobot` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gejala`
--

INSERT INTO `gejala` (`id`, `kode`, `gejala`, `bobot`) VALUES
(1, 'GJ01', 'Nyeri diperut atau pinggul', '0.4'),
(2, 'GJ02', 'Nyeri saat berhubungan intim', '-1.0'),
(3, 'GJ03', 'Sering mengalami anemia ketika haid karena banyak kehilangan darah', '1.0'),
(4, 'GJ04', 'Perut terasa penuh', '0.6'),
(5, 'GJ05', 'Adanya penekan pada pinggul', '0.6'),
(6, 'GJ06', 'Infertilitas atau keguguran', '0.6'),
(7, 'GJ07', 'Sembelit', '0.6'),
(8, 'GJ08', 'Terjadi pendarahan yang mengeluarkan banyak darah baik ketika masa menstruasi atau pun diluar menstruasi', '1.0'),
(9, 'GJ09', 'Terjadi penekan pada saluran indung telur yang membuat seorang wanita susah hamil', '0.6'),
(10, 'GJ10', 'Mengalami gangguan BAB, buang air kecil, pelebaran pembuluh darah vena dalam pinggul, gangguan ginjal karena adanya pembekakan pada pangkal tumor', '0.6'),
(11, 'GJ11', 'Biasanya mulai segera sebelum periode menstruasi, memuncak dalam 24 jam setelah timbulnya nyeri-nyeri dan menyerut lagi setelah sehari atau dua hari', '1.0'),
(12, 'GJ12', 'Sakit kepala dan/atau mual, yang dapat menjurus, meskipun jarang, pada titik muntah', '0.6'),
(13, 'GJ13', 'Mengalami suatu dorongan untuk membuang air kecil lebih sering kali', '0.4'),
(14, 'GJ14', 'Terjadi Penebalan  pada dinding rahim', '0.8'),
(15, 'GJ15', 'Sakit pada saat berhubungan seksual', '0.6'),
(16, 'GJ16', 'Sakit pada perut bagian bawah', '0.6'),
(17, 'GJ17', 'Sakit pada saat buang air besar', '0.6'),
(18, 'GJ18', 'Sakit pada bagian pinggang', '0.6'),
(19, 'GJ19', 'Saat menstruasi darah yang keluar lebih banyak', '1.0'),
(20, 'GJ20', 'Sakit terus menerus pada saat menstruasi', '1.0'),
(21, 'GJ21', 'Pendarahan saat berhubungan seksual', '1.0'),
(22, 'GJ22', 'Keluar cairan berwarna kekuningan dari vagina', '0.6'),
(23, 'GJ23', 'Sakit atau nyeri pada pinggul dan kaki', '1.0'),
(24, 'GJ24', 'Sakit  saat buang air besar', '0.8'),
(25, 'GJ25', 'Keluar urin tanpa noda saat tertawa, bersin atau batuk', '0.6'),
(26, 'GJ26', 'Usus bermasalah', '-1.0'),
(27, 'GJ27', 'Pendarahan menstruasi yang tidak teratur', '1.0'),
(28, 'GJ28', 'Periode menstruasi yang terlalu lama', '1.0'),
(29, 'GJ29', 'Pendarahan setelah berhubungan seksual', '1.0'),
(30, 'GJ30', 'Pendarahan pada vagina setelah menepouse', '0.4'),
(31, 'GJ31', 'Kemandulan biasa terjadi sebelum atau sesudah menopouse', '0.4'),
(32, 'GJ32', 'Pendarahan setelah berhubungan seksual', '0.1'),
(33, 'GJ33', 'Sakit saat berhubungan seksual', '-1.0'),
(34, 'GJ34', 'Mual dan muntah bersamaan dengan penyakit demam', '-1.0');

-- --------------------------------------------------------

--
-- Table structure for table `tmp_cf`
--

CREATE TABLE `tmp_cf` (
  `id` int(11) NOT NULL,
  `kode` varchar(45) DEFAULT NULL,
  `cf_total` varchar(45) DEFAULT NULL,
  `id_user` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tmp_cf`
--

INSERT INTO `tmp_cf` (`id`, `kode`, `cf_total`, `id_user`) VALUES
(1, 'GJ03', '0', 2),
(2, 'GJ05', '0', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tmp_id`
--

CREATE TABLE `tmp_id` (
  `id` int(11) NOT NULL,
  `id_jawab` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tmp_id`
--

INSERT INTO `tmp_id` (`id`, `id_jawab`) VALUES
(1, '3'),
(2, '5');

-- --------------------------------------------------------

--
-- Table structure for table `tmp_jawab`
--

CREATE TABLE `tmp_jawab` (
  `id` int(11) NOT NULL,
  `kode` varchar(45) DEFAULT NULL,
  `id_gejala` varchar(200) DEFAULT NULL,
  `cf_pakar` varchar(45) DEFAULT NULL,
  `jawaban` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tmp_jawab`
--

INSERT INTO `tmp_jawab` (`id`, `kode`, `id_gejala`, `cf_pakar`, `jawaban`) VALUES
(1, 'GJ03', 'Sering mengalami anemia ketika haid karena banyak kehilangan darah', '1.0', NULL),
(2, 'GJ05', 'Adanya penekan pada pinggul', '0.6', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(5) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `tanggal_lahir`, `alamat`, `username`, `password`) VALUES
(1, 'abc', '2018-03-01', 'abc', 'abc', 'abc'),
(2, '123', '2018-03-01', '', '123', '123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `diag`
--
ALTER TABLE `diag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gejala`
--
ALTER TABLE `gejala`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmp_cf`
--
ALTER TABLE `tmp_cf`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tmp_id`
--
ALTER TABLE `tmp_id`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmp_jawab`
--
ALTER TABLE `tmp_jawab`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `diag`
--
ALTER TABLE `diag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `gejala`
--
ALTER TABLE `gejala`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `tmp_cf`
--
ALTER TABLE `tmp_cf`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tmp_id`
--
ALTER TABLE `tmp_id`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tmp_jawab`
--
ALTER TABLE `tmp_jawab`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
